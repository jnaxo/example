<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\User;

class ProjectsTableSeeder extends Seeder
{
    const PROJECT_AMOUNT = 150;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = User::get('id');
        for ($i=0; $i < self::PROJECT_AMOUNT; $i++) { 
            $project = new Project();
            $project->name = $faker->company;
            $project->started_at = $faker->dateTime();
            $project->finished_at = $faker->dateTime();
            $project->budget = $faker->numberBetween(1000, 100000);
            $project->owner_id = $users->random()->id;
            $project->save();
        }
    }
}
