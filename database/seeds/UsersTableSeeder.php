<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    const USER_AMOUNT = 100;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // spanish config
        $faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
        // end config
        $genre = ['Masculino', 'Femenino'];

        for ($i=0; $i < self::USER_AMOUNT; $i++) { 
            $user = new User();
            $user->name = $faker->name;
            $user->lastname = $faker->lastname;
            $user->rut = $faker->dni;
            $user->npi = $faker->dni;
            $user->email = $faker->email;
            $user->genre = $genre[random_int(0, 1)];
            $user->password = bcrypt($faker->password);
            $user->save();
            $this->command->info('User ' . $user->name . ' created');
        }
        $this->command->info('success!');
    }
}
