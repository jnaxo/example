@extends('layouts.master')

@section('title', 'Lista de proyectos')

@section('content')
  <h1>Project index</h1>

  @if (session('message'))
      <div class="alert alert-success">
          {{ session('message') }}
      </div>
  @endif

  <div class="mt-3 mb-3">
    <a href="{{ route('projects.create') }}" class="btn btn-success">Nuevo Proyecto</a>
  </div>

  <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Título</th>
          <th scope="col">Encargado</th>
          <th scope="col">Fecha Inicio</th>
          <th scope="col">Fecha Término</th>
          <th scope="col">Presupuesto</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>

        @forelse ($projects as $project)
        <tr>
          <th scope="row">{{ $project->id }}</th>
          <td>{{ $project->name }}</td>
          <td>{{ $project->user->name }}</td>
          <td>{{ $project->started_at }}</td>
          <td>{{ $project->finished_at }}</td>
          <td>$ {{ $project->budget }}</td>
          <td>
              <a href="{{ route('projects.show', $project) }}" class="btn btn-sm btn-outline-info">Ver</a>
              <a href="{{ route('projects.edit', ['id' => $project->id]) }}" class="btn btn-sm btn-outline-warning">Editar</a>

              <form method="POST" action="{{ route('projects.destroy', $project) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-outline-danger">Eliminar</button>
              </form>

          </td>
        </tr>
        @empty
          <tr>
            <td>No hay registros</td>
          </tr>
        @endforelse

      </tbody>
    </table>
@endsection

@push('scripts')
    <script>
      // Custom JS
    </script>
@endpush

@push('styles')
    <!-- Custom Styles -->
@endpush
