@extends('layouts.master')

@section('title', 'Crear proyecto')

@section('content')
    <h1>Create Project</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('projects.store') }}">
        @csrf
        <div class="form-group">
            <label for="name">Nombre del proyecto</label>
            <input type="text" value="{{ old('name') }}" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="users">Responsable</label>
            <select class="form-control" id="users" name="owner_id">
                <option value="">Seleccione ...</option>

                @foreach ($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group">
            <label for="startedAt">Fecha de inicio</label>
            <input type="date" value="{{ old('started_at') }}" class="form-control" id="startedAt" name="started_at">
        </div>

        <div class="form-group">
            <label for="finishedAt">Fecha de término</label>
            <input type="date" value="{{ old('finished_at') }}" class="form-control" id="finishedAt" name="finished_at">
        </div>

        <div class="form-group">
            <label for="budget">Presupuesto</label>
            <input type="text" value="{{ old('budget') }}" class="form-control" id="budget" name="budget">
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
@endsection

@push('scripts')
    <script>
      // Custom JS
    </script>
@endpush

@push('styles')
    <!-- Custom Styles -->
@endpush
