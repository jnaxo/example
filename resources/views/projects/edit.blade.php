@extends('layouts.master')

@section('title', 'Modificar proyecto')

@section('content')
    <h1>Edit Project</h1>

    <a href="{{ route('projects.index') }}" class="btn btn-info mt-3 mb-3">Volver</a>

    <form method="POST" action="{{ route('projects.update', ['id' => $project->id]) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Título del proyecto</label>
            <input type="text" value="{{ $project->title }}" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="description">Descripción</label>
            <input type="text" value="{{ $project->description }}" class="form-control" id="description" name="description">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
@endsection

@push('scripts')
    <script>
      // Custom JS
    </script>
@endpush

@push('styles')
    <!-- Custom Styles -->
@endpush

