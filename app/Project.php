<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Project extends Model
{
    const TODO = 1;
    const WAITING = 2;
    const DONE = 3;

    protected $fillable = ['name', 'owner_id', 'started_at', 'finished_at', 'budget'];

    /* Relationships */
    public function user()
    {
        return $this->belongsTo('App\User', 'owner_id');
        // return $this->belongsTo(User::class, 'owner_id');
    }

    /* Accessors */
    public function getNameAttribute($value)
    {
        return strtolower($value);
    }

    public function getStartedAtAttribute($value)
    {
        // format with Carbon PHP
        $date = new Carbon($value);
        return $date->toFormattedDateString();
    }
    public function getFinishedAtAttribute($value)
    {
        // format with Carbon PHP
        $date = new Carbon($value);
        return $date->toFormattedDateString();
    }

    /* Muttator */
    // WIP
}
