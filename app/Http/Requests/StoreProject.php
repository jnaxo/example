<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'owner_id' => 'required|exists:users,id',
            'started_at' => 'nullable|date|after:yesterday',
            'finished_at' => 'nullable|date|after:started_at',
            'budget' => 'required|numeric',
        ];
    }
}
