<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProject;
use App\Project;
use App\User;
use Gate;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::orderBy('id')->get();

        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('projects.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\StoreProject  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProject $request)
    {
        Project::create($request->except('_token'));

        return redirect()->route('projects.index')->with([
            'message' => 'Su proyecto fue creado exitosamente'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $project = Project::findOrFail($id);
            dd($project->toArray());
        } catch (\Exception $e) {
            dd('Error!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
        Solo a modo de ejemplo.
        Se debería usar
        $project = Project::find($id);
         */

        $project = new Project(); // no necesario con BD
        $project->id = 12; // no necesario con BD
        $project->title = 'Curso Laravel';
        $project->description = 'Descripción del curso.';
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        Solo a modo de ejemplo.
        Se debería usar
        $project = Project::find($id);
         */
        $project = new Project(); // esta no corresponde con BD
        $project->title = $request->title;
        $project->description = $request->description;
        // $project->save(); cuando existe conexión a base de datos

        return redirect()->route('projects.index')->with([
            'message' => 'Su proyecto fue editado exitosamente'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $project = Project::findOrFail($id);

            if (Gate::denies('delete-project', $project)) {
                throw new \Exception('Error');
            }

            $project->delete();
            return redirect()->route('projects.index')->with([
                'message' => 'Su proyecto fue eliminado exitosamente'
            ]);
        } catch (\Exception $e) {
            return redirect()->route('projects.index')->with([
                'message' => 'Error al eliminar el proyecto'
            ]);
        }
    }
}
